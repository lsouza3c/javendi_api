class ChangeFacebookAccessTokenType < ActiveRecord::Migration
  def change
  	remove_column :users, :facebook_accesstoken, :string
  	add_column :users, :facebook_access_token, :text
  end
end
