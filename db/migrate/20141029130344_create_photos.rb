class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
    	t.references :ad
    	t.attachment :picture

      t.timestamps
    end
  end
end
