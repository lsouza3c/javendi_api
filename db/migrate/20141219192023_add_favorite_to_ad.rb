class AddFavoriteToAd < ActiveRecord::Migration
  def change
  	add_reference :ads, :favorite, index: true
  end
end
