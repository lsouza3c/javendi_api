class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :title
      t.float :price
      t.string :local
      t.text :description
      t.references :user

      t.timestamps
    end
  end
end
