class AddLocationToAds < ActiveRecord::Migration
  def change
  	add_column :ads, :latitude, :string
  	add_column :ads, :longitude, :string
  end
end
