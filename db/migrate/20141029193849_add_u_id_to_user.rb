class AddUIdToUser < ActiveRecord::Migration
  def change
  	add_column :users, :u_id, :string
  	add_column :users, :facebook_accesstoken, :string
  	add_column :users, :plain_password, :string
  end
end
