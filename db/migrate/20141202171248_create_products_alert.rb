class CreateProductsAlert < ActiveRecord::Migration
  def change
    create_table :products_alerts do |t|
    	t.references :user
    	t.string :keyword
    	
    	t.timestamps
    end
  end
end
