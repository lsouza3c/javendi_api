JaVendi::Application.routes.draw do
  devise_for :users

  devise_scope :user do 
  	post "/users/login_facebook", to: "devise/sessions#login_facebook", as: :login_facebook
  end
  
  resources :ads
  
  root to: 'ads#index'

  get "/login", to: "authentication#authenticate"
  get "/category", to: "ads#index_by_category"
  get "/my_ads", to: "ads#index_by_current_user"
  get "/last_added", to: "ads#index_last_added"
  get "/search", to: "ads#search_ad"

  get "/send_lost_password", to: "lost_password#send_email"
  post "/settings_prefs", to: "products_alerts#save_settings_prefs"
  post "/favorite_ad", to: "favorites#favorite_ad"
  get "/my_favorite_ads", to: "favorites#favorited_by_current_user"
  post "/upload_ad_image", to: "photos#create"
  delete "/remove_ad_image", to: "photos#destroy"
end
