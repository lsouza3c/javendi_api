class AdsController < ApplicationController
	skip_before_filter :http_basic_authenticate, only: [:index_by_category, :index, :index_last_added, :show, :search_ad, :favorite_ad]

	before_filter :set_ad, only: [:edit]
	def index
		@ads = Ad.all
		@ads.each do |ad|
			ad.contact = ad.user.name
		end
   	respond_to do |format|
      format.json
      format.html
    end
	end

	def show
		@ad = get_ad(params[:id])
		respond_to do |format|
			format.json
		end
	end

  def edit
  end

	def new
		@ad = Ad.new
		6.times do
    		photo = @ad.photos.build
  		end
		respond_to do |format|
			format.json {render :json => @ad}
			format.html
		end
	end

	def create
		@ad = Ad.new(ad_params)
		if params[:ad][:categories_json].present?
			aux = params[:ad][:categories_json].split(",")
			aux.each do |id|
				@ad.categories << Category.find(id)
			end
		end
		respond_to do |format|
			if @ad.save
				format.html {redirect_to root_path}
				format.json
			else
				format.json {render :json => @ad.errors, status: :unprocessable_entity}
			end
		end
	end

	def update
		@ad = get_ad(params[:ad][:id])
		if params[:ad][:categories_json].present?
			@ad.categories.clear
			aux = params[:ad][:categories_json].split(",")
			aux.each do |id|
				@ad.categories << Category.find(id)
			end
		end
		# @ad.photos = []
		respond_to do |format|
			if @ad.update_attributes(ad_params)
				format.json
			else
				format.json {render json: @ad.erros,  status: :unprocessable_entity}
			end
		end
	end

	def destroy
		@ad = get_ad(params[:id])
		@ad.destroy
		respond_to do |format|
			format.json { render json: [], status: :ok } 
		end
	end

	def index_by_category
		ads = Ad.find_by_categories_name(params[:name])
		@ads = []
		@favorites = []
		if current_user.present?
			favorites = current_user.favorites
			ads.each do |ad|
				@ads << ad if ad.user_id != current_user.id
			end 
			
			favorites.each do |fa|
				@favorites << Ad.find(fa.ad_id)
			end

			@favorites.each do |fa|
				@ads.delete(fa)
			end
		else 
			ads.each do |ad|
				@ads << ad
			end 
		end

		respond_to do |format|
      		format.json
      		format.html
    	end
	end

	def index_by_current_user
		@ads = current_user.ads
		respond_to do |format|
      format.json
    end
	end

	def index_last_added
		@ads = Ad.order("created_at DESC").limit(10)
		respond_to do |format|
      format.json
    end
	end

	def search_ad
		@ads = Ad.search_ads(params[:key_word], params[:category_name], params[:local], params[:min], params[:max])
		respond_to do |format|
      format.json
    end
	end

	def favorite_ad
		@ad = get_ad(params[:ad][:id])
		@favorite = Favorite.new
		@favorite = @ad.favorites.build 
		puts(current_user.id)
		@favorite.user_id = current_user.id
		if @favorite.save
			respond_to do |format|
				format.json { render json: [], status: :ok } 
			end
		else
			respond_to do |format|
				format.json { render json: @ad.errors, status: 403 } 
			end
		end
	end

	def favorited_by_current_user
		@ads = current_user.favorites
		@favorite_ads = []
		@ads.each do |ad|
			@favorite_ads << get_ad(ad[:id])
		end
		respond_to do |format|
      format.json
    end
	end

	private
	
	def ad_params
   	params.require(:ad).permit(:user_id, :title, :categories_json, :price, :local, :description, :latitude, :longitude, :category_ids => [],
   	 :photos_attributes	=> [:ad_id, :picture])
  end

  	def set_ad
  		@ad = Ad.find(params[:id])
  	end

  	def get_ad(ad_id)
  		Ad.find(ad_id)
  	end
end
