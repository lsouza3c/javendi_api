class FavoritesController < ApplicationController
	def favorited_by_current_user
		favorites = current_user.favorites
		@ads = []
		favorites.each do |fa|
			@ads << Ad.find(fa.ad_id)
		end
		
		respond_to do |format|
			format.json
		end
	end

	def favorite_ad	
		@favorite = Favorite.new(ad_params)
		if @favorite.save
			respond_to do |format|
				format.json { render json: [], status: :ok } 
			end
		end
	end

	def ad_params
    	params.require(:favorite).permit(:user_id, :ad_id)
  	end
end