class LostPasswordController < ApplicationController
  skip_before_filter :http_basic_authenticate, only: [:send_email]
  skip_before_filter :verify_authenticity_token, only: [:send_email]

  def send_email
    generated_password = Devise.friendly_token.first(8)
    @user = User.find_by_email(params[:email])
    if @user
      @user.update_attribute(:password, generated_password)
      LostPasswordMailer.lost_password(@user, generated_password).deliver

      respond_to do |format|
        format.all { head :no_content }
      end
      else
      respond_to do |format|
        format.all { head :not_found }
      end
    end
  end
end