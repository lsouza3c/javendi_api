class PhotosController < ApplicationController

	skip_before_filter :http_basic_authenticate

	def create
		@photo = Photo.new(photo_params)
		respond_to do |format|
			if @photo.save
				format.html {redirect_to root_path}
				format.json
			else
				format.json {render :json => @photo.errors, status: :unprocessable_entity}
			end
		end
	end

	def destroy
		@photo = Photo.find(params[:id])
		@photo.destroy
		respond_to do |format|
			format.json { render json: [], status: :ok } 
		end
	end

	def photo_params
   	params.require(:photo).permit(:ad_id, :picture)
  end
end
