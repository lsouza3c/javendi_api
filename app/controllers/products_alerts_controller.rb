class ProductsAlertsController < ApplicationController
	skip_before_filter :http_basic_authenticate

# contact_pref:
# 0 - Ligação
# 1 - SMS
# 2 - Email
# Parameters: {	"keywords"=>["oi", "tudo", "bem", "com", "voce"], 
# 							"user"=>{"email"=>"0@0.com", "id"=>"3", "name"=>"WO LO LO", "password"=>"[FILTERED]", "phone"=>"12312312"}}
  def save_settings_prefs
		@user = User.find(params[:user][:id])
		@user[:contact_pref] = params[:user][:contact_pref]
		@user.products_alerts = []

		if params[:keywords].present?
			params[:keywords].each do |keyword|
				@product_alert = ProductsAlert.new
				@product_alert.user_id = params[:user][:id]
				@product_alert.keyword = keyword
				@user.products_alerts << @product_alert	
			end
		end

		if @user.update_attributes(user_params)
			respond_to do |format|
				format.json
			end
		else
			respond_to do |format|
				format.all { head :bad_request }
			end
		end
 	end

	def user_params
    params.require(:user).permit(:password, :password_confirmation, :contact_pref, :products_alerts_attributes  => [:keyword, :user_id])
  end
end