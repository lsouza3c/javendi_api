class Devise::SessionsController < DeviseController
  prepend_before_filter :require_no_authentication, only: [ :new, :create ]
  prepend_before_filter :allow_params_authentication!, only: :create
  prepend_before_filter only: [ :create, :destroy ] { request.env["devise.skip_timeout"] = true }
  skip_before_filter :http_basic_authenticate, only: [:login_facebook, :create, :destroy]
  protect_from_forgery except: [:login_facebook, :create]
  skip_before_filter :verify_authenticity_token, only: [:login_facebook, :index, :destroy]


  # GET /resource/sign_in
  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    respond_with(resource, serialize_options(resource))
  end

  # POST /resource/sign_in
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    
    respond_to do |format|
      format.json
      format.html{ respond_with resource, location: after_sign_in_path_for(resource) }
    end
  end

  # DELETE /resource/sign_out
  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    yield resource if block_given?

    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to redirect_path }
    end
  end

  def login_facebook
    # @user = User.find_by_email(user_params[:email])
    @user = User.find_by_email(facebook_user_params(params)[:email])
    if @user.present?
      # if @user.update(user_params)
      if @user.update(facebook_user_params(params))
        respond_to do |format|
          format.json
        end
      else
        render json: current_user.errors, status: :unprocessable_entity
      end
    else
      # @user = User.new(user_params)
      @user = User.new(facebook_user_params(params))
      if @user.save
        respond_to do |format|
          format.json
        end
      else
        render json: current_user.errors, status: :unprocessable_entity
      end
    end
  end

  private
  
  def user_params
    params.require(:user).permit(:name, :email, :phone, :contact_pref, :u_id, :facebook_access_token, :avatar,
      :products_alerts_attributes  => [:keyword])
  end

  def facebook_user_params(params)
    return {
      name: params[:name],
      email: params[:email],
      password: Devise.friendly_token.first(8),
      u_id: params[:id],
      facebook_access_token: params[:facebook_access_token],
      avatar: "https://graph.facebook.com/" + params[:id] + "/picture?type=normal"
    }
  end

  protected

  def sign_in_params
    devise_parameter_sanitizer.sanitize(:sign_in)
  end

  def serialize_options(resource)
    methods = resource_class.authentication_keys.dup
    methods = methods.keys if methods.is_a?(Hash)
    methods << :password if resource.respond_to?(:password)
    { methods: methods, only: [:password] }
  end

  def auth_options
    { scope: resource_name, recall: "#{controller_path}#new" }
  end
end
