class Ad < ActiveRecord::Base
	belongs_to :user
	has_and_belongs_to_many :categories
	has_many :photos, :dependent => :destroy
  belongs_to :favorite
	attr_accessor :categories_json, :contact

	accepts_nested_attributes_for :photos
  validates_presence_of :user

	scope :find_by_categories_name, lambda { |category_name| where(categories: {name: category_name}).joins(:categories)}
  scope :search_ad, lambda { |key_word, category_name| joins(:categories).where(categories: {name: category_name}).where("ads.title like '%#{key_word}%'") }

  def self.search_ads(key_word = nil, category_name = nil, local = nil, min = nil, max = nil)
    result = Ad
    result = result.joins(:categories).where(categories: {name: category_name}) if category_name.present?
    result = result.where("ads.title like '%#{key_word}%'") if key_word.present?
    result = result.where("local like '%#{local}%'") if local.present?
    result = result.where("price >= ?", min) if min.present?
    result = result.where("price <= ?", max) if max.present?
    return result
  end

  def self.filter_by_current_user(current_user_id)
    joins("LEFT OUTER JOIN favorites ON favorites.favorable_id = ads.id AND favorites.favorable_type = 'Ad'").where("ads.user_id not like ? && favorites.user_id not like ?", current_user_id, current_user_id)
  end
end