class Photo < ActiveRecord::Base
	belongs_to :ad, :polymorphic => true
	has_attached_file :picture, :styles => { :small => "56x56>", :thumb => "100x100#" }, 
  									:default_url => "/images/:style/missing.png", 
  									:url => "/assets/ja_vendi/:id/:style/:basename.:extension",
  									:path => ":rails_root/public/assets/ja_vendi/:id/:style/:basename.:extension"

  validates_attachment_content_type :picture, :content_type => [/\Aimage\/.*\Z/, "application/octet-stream"]
end
