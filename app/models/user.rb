class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :ads, :dependent => :destroy
  has_many :products_alerts, :dependent => :destroy
  has_many :favorites

  accepts_nested_attributes_for :products_alerts

  before_validation :check_password

  has_attached_file :avatar, :styles => { :small => "56x56>", :thumb => "100x100#" }, 
  									:default_url => "/images/:style/missing.png", 
  									:url => "/assets/ja_vendi/:id/:style/:basename.:extension",
  									:path => ":rails_root/public/assets/ja_vendi/:id/:style/:basename.:extension"

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def check_password
  	if self.password.blank?
  		self.plain_password = self.password = SecureRandom.hex
  	else
  		self.plain_password = SecureRandom.hex
  	end
  end
end
