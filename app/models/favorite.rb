class Favorite < ActiveRecord::Base
  has_one :ad
  belongs_to :user
end
