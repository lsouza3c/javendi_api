class LostPasswordMailer < ActionMailer::Base
  default from: "noreply@javendi.com"

  def lost_password(user, password)
    @user = user
    @password = password
    mail(to: @user.email, subject: 'Perdeu a senha?')
  end
end
