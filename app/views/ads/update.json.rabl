object @ad
attributes :id, :title, :price, :local, :description, :latitude, :longitude
node(:contact) {|ad| ad.user.name}
child(:categories) {attributes :name}
child(:photos) {attributes :picture}
child(:user) {attribute :name, :email, :phone, :contact_pref}