collection @ads, root: "ads", object_root: false
attributes :id, :title, :price, :local, :description, :contact, :latitude, :longitude
child(:categories) {attributes :name}
child(:photos) {attribute :picture, :id}
child(:user) {attribute :name, :email, :phone, :contact_pref}