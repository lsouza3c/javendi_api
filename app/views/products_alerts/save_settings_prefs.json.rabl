object @user
attributes :id, :name, :email, :phone, :password, :contact_pref, :avatar
child(:products_alerts) { attributes :keyword }