object @user
attributes :id, :name, :email, :password, :contact_pref, :avatar, :phone
child(:products_alerts) { attributes :keyword }
