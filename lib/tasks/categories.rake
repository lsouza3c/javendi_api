#encoding: utf-8
include ActionView::Helpers::UrlHelper

namespace :categories do 
	desc "Create categories"
	task :create => :environment do
		categories = ["Celulares", "Tablets", "Eletronicos", "Video-Games", "Informatica", "Esportes", "Arte e Lazer", "Veiculos", "Para Casa", "Roupas", "Bebes", "Coisas"]

		categories.each do |category|
			Category.create(name: category)
		end
	end
end